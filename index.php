<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>App that counts ways to fill the tank...</h2>
    <div class="col-md-12" style="margin-top: 50px;padding-left: 0;">
        <form>
            <div class="col-md-5" style="padding-left: 0;">
                <div class="form-group">
                    <label for="tankSize">Tank size:</label>
                    <input type="text" class="form-control" id="tankSize"/>
                </div>
                <button type="button" class="btn btn-primary" id="send">Count</button>
                <button style="margin-left:10px;" type="reset" class="btn btn-danger" onclick="document.location.reload()">Reset</button>
                <p style="margin-top: 10px;">You can see json format response in DevTools network tab</p>
            </div>
        </form>
    </div>
</div>

<script>
    $("#send").on('click',function(){

        var min = 10;
        var max = 200;

        var tankSize = $("#tankSize").val();

        if((tankSize < min) || (tankSize > max)){
            alert('Number must be between 10 and 200');
            return false;
        }

        if($.isNumeric(tankSize)){
            //works
        }else {
            alert('Only numbers are allowed');
            return false;
        }

        $.ajax({
            type: "GET",
            url: "rest_service.php",
            data: "tankSize=" + tankSize,
            async: true,
            cache: false,
            dataType: 'json',
            success: function(response){

            },
            error: function(){
                console.log("error");
            }
        });
    });
</script>

</body>
</html>
