<?php


        if(isset($_GET['tankSize'])) {

            $tankSize = (int)$_GET['tankSize'];

        }

        function howmanyways($tankSize){
            //57
            $buckets = [
                10,5,2,1
            ];

            $results = [];

            foreach ($buckets as $bucket) {

                $result = $tankSize / $bucket;//78 / 10

                if(is_float($result)) {

                    // 2 nd degree counting

                    $whole = (int)floor($result); //7

                    $floatPart = $tankSize - ($whole * $bucket); // 8

                    foreach ($buckets as $item) {

                        if($floatPart >= $item) {

                            $result2 = $floatPart / $item;//8/5


                            if(is_float($result2)) {

                                // 3rd degree counting

                                $whole2 = (int)floor($result2);//1

                                $floatPart2 = $floatPart - ($whole2 * $item);//3

                                foreach ($buckets as $item2) {

                                    if($floatPart2 >= $item2){

                                        $result3 = $floatPart2 / $item2;//3/2

                                        if(is_float($result3)) {
                                            // 4th degree

                                            $whole3 = (int)floor($result3);//1

                                            $floatPart3 = $floatPart2 - ($whole3 * $item2);//1
                                            foreach ($buckets as $item3) {
                                                if ($floatPart3 >= $item3) {

                                                    $result4 = $floatPart3 / $item3;// 1 / 1

                                                    $results[] = $whole . ' * ' . $bucket . ' lt + ' . $whole2 . ' * ' . $item . ' lt +'
                                                        . $whole3 . ' * ' . $item2 . ' lt + '.$result4. ' * ' .$item3.' lt';
                                                }
                                            }

                                        }else {


                                            $results[] = $whole . ' * ' . $bucket . ' lt + ' . $whole2 . ' * ' . $item . ' lt +'
                                                . $result3 . ' * ' . $item2 . ' lt';
                                        }
                                    }
                                }

                            }
                            else {

                                $results[] = $whole . ' * ' . $bucket . ' lt + ' . $result2 . ' * ' . $item . ' lt';
                            }
                        }
                    }

                }
                else {
                    // 1 st degree counting
                    $results[] = $result.' * '.$bucket.' lt';
                }

            }


            $count = count($results);
            $array = [
                'Count' => $count,
                'Log' => $results,

            ];
            echo json_encode($array);
        }

        howmanyways($tankSize);
?>